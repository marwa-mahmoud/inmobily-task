# YotuTube Task

Hi, I'm Marwa Mahmoud and this is my task. I used Vue.JS for the task which is Implement a SPA single page application displaying list of videos in a YouTube channel.


## Build Setup

```bash
#install dependencies
npm install

#serve with hot reload at localhost:8080
npm run dev
```

- Moment.js 
> To parse, validate, manipulate, and display dates and times in JavaScript [see](https://momentjs.com/)
- VueRouter
- VueOffline
> This library allows you to enhance offline capabilities of your Vue.js application. It's especially useful when you're building offline-first Progressive Web Apps or just want to inform your users that they lost internet connection [see](https://github.com/filrak/vue-offline)

## Youtube API

I used youtube data API to list videos from a specific channel.
This API you can call to list videos :
```bash
https://www.googleapis.com/youtube/v3/search?key=AIzaSyDaRn_LAMPRDP3itjZTENzxquH2ytv0MZo&channelId=UCyIe-61Y8C4_o-zZCtO4ETQ&part=snippet,id&maxResults=10
```
here is the response will be like this:
```bash
{
"kind": "youtube#searchListResponse",
"etag":""SJZWTG6xR0eGuCOh2bX6w3s4F94/_BocQq1YV3295bqyrx13tbJ3xd0"",
"nextPageToken": "CAoQAA",
"pageInfo": {
    "totalResults": 360,
    "resultsPerPage": 10
},
"items": [
    {
        "kind": "youtube#playlist",
        "etag": "\"XpPGQXR4Qk/XsatNRtxJQ\"",

        .........
        .........
```
and this is the api i cold to get the next page for pagination:

```bash 
https://www.googleapis.com/youtube/v3/search?key=AIzaSyDaRn_LAMPRDP3itjZTENzxquH2ytv0MZo&channelId=UCyIe-61Y8C4_o-zZCtO4ETQ&part=snippet,id&maxResults=10&pageToken=${nextPageToken}
```
**"nextPageToken"**  is in the response just send as a parameter in each time to get the next page.
after the 1st call the response will contain **"prevPageToken"** also pass it to get the previous response with this token.

to get the details of the video such as (Title - Upload date - Duration - # of likes - # of views - Description - Thumbnail) 
 ``` bash
 https://www.googleapis.com/youtube/v3/videos?part=statistics&id=${video_id}&key=AIzaSyDaRn_LAMPRDP3itjZTENzxquH2ytv0MZo`
 ```
${Video_id}  = 1h9_cB9mPT8 
this video id will change each time you pass when you click on details button.
  

## Search by title

This function i used to search by title:
```bash
filteredSearchItems() {

	let  self  =  this;
	return  this.videos.filter(i  => {
		return (!self.search || i.snippet.title.toString().toLowerCase().includes(self.search.toString().toLowerCase()));});}
```

