import homePage from '@/modules/youtubeVedios/homePage.vue';
import detailsPage from '@/modules/youtubeVedios/detailsPage.vue'


export default [
	{
		path: '/',
		name: 'homeSearch',
		component: homePage,
	},
	{
		path: '/search-results/:id',
		name: 'searchResults',
		component: detailsPage
	},

]