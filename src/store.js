import Vue from "vue";

import Vuex from "vuex";
import createPersistedState from "vuex-persistedstate";
Vue.use(Vuex);

// Modules

import User from "./store/user";


export default new Vuex.Store({
  plugins: [createPersistedState()],
  modules: {
    user: User,
  }
});
