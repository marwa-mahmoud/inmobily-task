// src/main.js
import "babel-polyfill";
import Vue from "vue";
import VueRouter from "vue-router";
import store from "./store";
import VueToast from "vue-toast-notification";
import "vue-toast-notification/dist/index.css";
import "./plugins/veeValidate";
import VueOffline from 'vue-offline'

// CASL
import { abilitiesPlugin } from "@casl/vue";
Vue.use(abilitiesPlugin);

// Vuex
import Vuex from "vuex";
Vue.use(Vuex);
Vue.use(VueToast);
Vue.use(VueOffline)
// Vuetify
import vuetify from "@/plugins/vuetify";

// fontawsome
import { library } from "@fortawesome/fontawesome-svg-core";
import { faUserSecret } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import moment from 'moment';



library.add(faUserSecret);

Vue.component("font-awesome-icon", FontAwesomeIcon);


// Date Format

Vue.filter('formatDate', function(value) {
  if (value) {
    return moment(String(value)).format('DD/MM/YYYY')
  }
});

Vue.filter('formatTime', function(value) {
  if (value) {
    return moment(String(value)).format('H:mm:ss')
  }
});

// My App
import App from "./App.vue";
import router from "./router";

// My router
Vue.config.productionTip = false;
Vue.use(VueRouter);

new Vue({
  render: h => h(App),
  vuetify,
  store,
  router
}).$mount("#app");
