import Vue from 'vue';
import i18n from './i18l';
import {
	extend,
	configure,
	ValidationProvider,
	ValidationObserver
} from "vee-validate";
import { required, email, min } from "vee-validate/dist/rules";

// Shared Components
Vue.component("ValidationProvider", ValidationProvider);
Vue.component("ValidationObserver", ValidationObserver);

configure({
	defaultMessage: (field, values) => {
		// override the field name.
		values._field_ = i18n.t(`fields.${field}`);

		return i18n.t(`validation.${values._rule_}`, values);
	}
});

extend("required", required);
extend("min", min);
extend("email", email);
