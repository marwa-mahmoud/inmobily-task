import Router from 'vue-router';
import authRoutes from '@/routes/authRoutes';

// import store from "./store.js";

const baseRoutes = [];

const routes = baseRoutes.concat(authRoutes);

const router = new Router({
	mode: 'history',
	routes
})

export default router;